use db_restoran_meni;

insert into kategorija 
	(id, name)
    values (1, 'kategorija 1');

insert into kategorija 
	(id, name)
    values (2, 'kategorija 2');

insert into kategorija 
	(id, name)
    values (3, 'kategorija 3');

insert into meni_item
	(kategorija_id, name, price)
    values (1, 'meni_item 1', 150);

insert into meni_item
	(kategorija_id, name, price)
    values (1, 'meni_item 2', 160);

insert into meni_item
	(kategorija_id, name, price)
    values (1, 'meni_item 3', 140);

insert into meni_item
	(kategorija_id, name, price)
    values (1, 'meni_item 4', 170);
    
insert into meni_item
	(kategorija_id, name, price)
    values (2, 'meni_item 5', 130);

insert into meni_item
	(kategorija_id, name, price)
    values (2, 'meni_item 6', 180);

insert into meni_item
	(kategorija_id, name, price)
    values (2, 'meni_item 7', 120);

insert into meni_item
	(kategorija_id, name, price)
    values (2, 'meni_itema 8', 190);


-- insert users
-- password is 12345 (bcrypt encoded) 
insert into security_user (username, password, first_name, last_name, role) values 
	('admin', '$2a$04$4pqDFh9SxLAg/uSH59JCB.LwIS6QoAjM9qcE7H9e2drFuWhvTnDFi', 'Admin', 'Admin', 'ADMINISTRATOR');
-- password is abcdef (bcrypt encoded)
insert into security_user (username, password, first_name, last_name, role) values 
	('petar', '$2a$04$Yr3QD6lbcemnrRNLbUMLBez2oEK15pdacIgfkvymQ9oMhqsEE56EK', 'Petar', 'Petrovic', 'WORKER');
