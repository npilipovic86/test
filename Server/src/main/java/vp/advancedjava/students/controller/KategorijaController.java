package vp.advancedjava.students.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import vp.advancedjava.students.model.Kategorija;
import vp.advancedjava.students.service.KategorijaService;

@RestController
public class KategorijaController {
	
	@Autowired
	KategorijaService kategorijaService;
	
	
    @GetMapping(value = "api/kategorije")
    public ResponseEntity<List<Kategorija>> get() {
        final List<Kategorija> kategorije = kategorijaService.findAll();
        return new ResponseEntity<>(kategorije,HttpStatus.OK);
    }

}
