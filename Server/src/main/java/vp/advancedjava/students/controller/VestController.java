package vp.advancedjava.students.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import vp.advancedjava.students.dto.ResponseDTO;
import vp.advancedjava.students.model.Vest;
import vp.advancedjava.students.service.VestService;

@RestController
public class VestController {
	
	@Autowired
	VestService vestService;
	
	
//	    @GetMapping(value = "api/vest",params = "name")
//		public Page<Vest> getVestByNameContains(
//				@RequestParam String name,Pageable page) {
//	    	Page<Vest> vest = vestService.findByNameContains(name,page);
//
//			return vest;
//		}
	
	
	  @GetMapping(value = "api/vest")
	    public Page<Vest> getPage(Pageable pageable) {
	        final Page<Vest> meniItems = vestService.findAll(pageable);
	        return meniItems;
	    }
	  
	  
	  @GetMapping(value = "api/vest/{id}")
	    public Vest getVest(@PathVariable Long id) {
	        final Vest meniItems = vestService.findOne(id);
	        return meniItems;
	    }

//	    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR')")
	    @PostMapping(value = "api/vest")
	    public ResponseEntity<Vest> create(@RequestBody Vest meniItem) {
	        final Vest savedMeniItem = vestService.save(meniItem);
	        return new ResponseEntity<>(savedMeniItem, HttpStatus.CREATED);
	    }

	    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR')")
	    @DeleteMapping(value = "api/vest/{id}")
	    public ResponseEntity<ResponseDTO> delete(@PathVariable Long id) {
	        final Vest meniItem = vestService.findOne(id);
	        if (meniItem == null) {
	            return new ResponseEntity<>(new ResponseDTO("Not found"), HttpStatus.NOT_FOUND);
	        }

	        vestService.remove(id);
	        return new ResponseEntity<>(new ResponseDTO("OK"), HttpStatus.OK);
	    }
	    
	    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR')")
	    @PutMapping(value = "api/vest/{id}")
	    public ResponseEntity<Vest> update(@PathVariable Long id,
	                                 @RequestBody Vest meniItem) {
	        final Vest foundItem = vestService.findOne(id);

	        if (foundItem == null) {
	            return new ResponseEntity<>(meniItem, HttpStatus.NOT_FOUND);
	        }

	        foundItem.setName(meniItem.getName());
	        foundItem.setKategorija(meniItem.getKategorija());
	        foundItem.setOpis(meniItem.getOpis());
	        foundItem.setSadrzaj(meniItem.getSadrzaj());

	        final Vest foundMeniItem = vestService.save(foundItem);
	        return new ResponseEntity<>(foundMeniItem, HttpStatus.OK);
	    }
	

}
