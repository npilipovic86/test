package vp.advancedjava.students.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import vp.advancedjava.students.model.Kategorija;

@Component
public interface KategorijaRepository extends JpaRepository<Kategorija, Long> {

}
