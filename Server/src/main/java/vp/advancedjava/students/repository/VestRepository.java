package vp.advancedjava.students.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import vp.advancedjava.students.model.Vest;

@Component
public interface VestRepository extends JpaRepository<Vest, Long> {
	public Page<Vest> findByNameContains(String naslov,Pageable page);
}
