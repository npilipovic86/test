package vp.advancedjava.students.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Kategorija {
	
	@Id
    @GeneratedValue
    private Long id;
	
	private String name;
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Kategorija(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Kategorija() {
		super();
		// TODO Auto-generated constructor stub
	}
	

}
