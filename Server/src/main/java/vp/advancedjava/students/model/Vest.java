package vp.advancedjava.students.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Vest {
	
	
	@Id
    @GeneratedValue
    private Long id;
	
	private String name;
	
	@ManyToOne(fetch = FetchType.EAGER) 
	Kategorija kategorija;
	
	private String opis;
	private String sadrzaj;
	
	
	public Vest() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Kategorija getKategorija() {
		return kategorija;
	}
	public void setKategorija(Kategorija kategorija) {
		this.kategorija = kategorija;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public String getSadrzaj() {
		return sadrzaj;
	}
	public void setSadrzaj(String sadrzaj) {
		this.sadrzaj = sadrzaj;
	}
	public Vest(Long id, String name, Kategorija kategorija, String opis, String sadrzaj) {
		super();
		this.id = id;
		this.name = name;
		this.kategorija = kategorija;
		this.opis = opis;
		this.sadrzaj = sadrzaj;
	}
	

}
