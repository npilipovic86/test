package vp.advancedjava.students.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vp.advancedjava.students.model.Kategorija;
import vp.advancedjava.students.repository.KategorijaRepository;

@Component
public class KategorijaService {

	
	
	@Autowired
	KategorijaRepository kategorijeRepository;
	
	
	
	public List<Kategorija> findAll() {
		return kategorijeRepository.findAll();
	}
}
