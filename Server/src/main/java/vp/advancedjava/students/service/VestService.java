package vp.advancedjava.students.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import vp.advancedjava.students.model.Vest;
import vp.advancedjava.students.repository.VestRepository;



@Component
public class VestService {
	
	@Autowired
	VestRepository vestRepository ;
	
	
	public List<Vest> findAll() {
		return vestRepository.findAll();
	}
	
	public Page<Vest> findAll(Pageable page) {
		return vestRepository.findAll(page);
	}

	public Vest findOne(Long id) {
		return vestRepository.findOne(id);
	}
	public  Page<Vest> findByNameContains(String name,Pageable page){
		return vestRepository.findByNameContains(name, page);	
	};

	public Vest save(Vest meniItem) {
		return vestRepository.save(meniItem);
	}

	public void remove(Long id) {
		vestRepository.delete(id);
	}

}
