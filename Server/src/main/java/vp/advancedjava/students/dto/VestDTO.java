package vp.advancedjava.students.dto;



import vp.advancedjava.students.model.Kategorija;
import vp.advancedjava.students.model.Vest;

public class VestDTO {
	
	
    private Long id;
    
    private String name;

	private String opis;
	private String sadrzaj;
    
    Kategorija kategorija;
	
	public VestDTO(Long id, String name, Kategorija kategorija, String opis, String sadrzaj) {
		super();
		this.id = id;
		this.name = name;
		this.kategorija = kategorija;
		this.opis = opis;
		this.sadrzaj = sadrzaj;
	}
	
	public VestDTO(Vest vest) {
		super();
		this.id = vest.getId();
		this.name = vest.getName();
		this.kategorija = vest.getKategorija();
		this.opis = vest.getOpis();
		this.sadrzaj = vest.getSadrzaj();
	}
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}




	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Kategorija getKategorija() {
		return kategorija;
	}
	public void setKategorija(Kategorija kategorija) {
		this.kategorija = kategorija;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public String getSadrzaj() {
		return sadrzaj;
	}
	public void setSadrzaj(String sadrzaj) {
		this.sadrzaj = sadrzaj;
	}
	
	

}
