import { Kategorija } from "app/model/kategorija";



export interface Vest {


    id?: number;
    name: string;
    kategorija: Kategorija;
    opis:string;
    sadrzaj:string;

}