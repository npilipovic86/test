// import { kategorijaService } from './kategorija.service';
// import { meniItemService } from './meniItem.service';
// import { meniItem } from './../model/meniItem';
// import { Kategorija } from 'app/model/kategorija';
import { Component, OnInit } from '@angular/core';
import { Page } from 'app/model/page';
import { AuthenticationService } from 'app/service/authentication-service.service';
import { VestService } from 'app/meni-component/vest.service';
import { KategorijaService } from 'app/meni-component/kategorija.service';
import { Vest } from 'app/model/vest';
import { Kategorija } from 'app/model/kategorija';

@Component({
  selector: 'app-meni-component',
  templateUrl: './meni-component.component.html',
  styleUrls: ['./meni-component.component.css']
})
export class MeniComponentComponent implements OnInit {
  
  kategorije: Kategorija[];


  
    page: Page<Vest>;
  
    currentPageNumber: number;
  
    vest: Vest;
  
    forEdit: Vest;
  
    forSearch: Vest;
  
    nameItem: string;
  



  ngOnInit() {
    this.currentPageNumber = 0;
    this.loadData();
    this.vestService.getVesti().subscribe(
      (vest) => {
        this.vest = vest;
      }
    );
    this.kategorijaService.getKategorije().subscribe(
      (kategorija) => {
        this.kategorije = kategorija;
      }
    );
  }

  constructor(private authenticationService: AuthenticationService,private vestService: VestService,private kategorijaService: KategorijaService) { }



  changePage(i: number) {
    this.currentPageNumber += i;
    this.loadData();
  }

  isAdmin(): boolean {
    return this.authenticationService.isAdmin();
  }

  loadData() {
    this.vestService.getVestiPage(this.currentPageNumber).subscribe(data => {
      this.page = data;
      
    })
  }

  addVest(vest: Vest) {
    this.vestService.saveVest(vest).subscribe(
      (savedComp) => {
        this.loadData();
      }
    )
  }

  delete(vest: Vest) {
    this.vestService.deleteVest(vest).subscribe(
      (response) => {
        this.loadData();
      },
      (error) => {
        console.log('This is not good!', 'Oops!');
      }
    );
  }

  edit(meniItem: Vest) {
    //kopija objekta comp
    this.forEdit = Object.assign({}, meniItem);
  }

  find() {
    this.vestService.filterByName(this.nameItem, this.currentPageNumber).subscribe(
      (data) => {
        this.page = data;

        
      }
    )
  }

}
